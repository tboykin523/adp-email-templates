ADP Email Templates

Tools & Resources

ADP Email Templates Bitbucket Repo
https://bitbucket.org/tboykin523/adp-email-templates/src/master/


HTML Minifier
https://www.willpeavy.com/tools/minifier/


UnCSS (remove unused CSS)
https://uncss-online.com/


Responsive Email CSS Inliner
https://htmlemail.io/inline/


The Ultimate Guide to Bulletproof Buttons in Email Design
https://www.litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design/


Foundation for Emails 2
https://get.foundation/emails.html


Free Responsive Simple HTML Email Template
https://github.com/leemunroe/responsive-html-email-template